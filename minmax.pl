% ranged vs melee
% character properties: {name, hp, range, attk, pos}
% input: 2 characters, steps
% output: optimal steps for the first character
% TEST: step_children(15, max, {lefty, 10, 5, 1, 3}, {righty, 10, 1, 2, 6}, [start], OptSteps, OptVal).

steps_allowed([attk, withd, idle, appr]).

% step(STEP, ACTOLD, PASSOLD, ACTNEW, PASSNEW).
% IDLE
step(idle, Act, Pass, Act, Pass).

% ATTK
step(attk, {ActName, ActHp, ActRange, ActAttk, ActPos},
    {PassName, PassHp, PassRange, PassAttk, PassPos},
    {ActName, ActHp, ActRange, ActAttk, ActPos},
    {PassName, PassHpNew, PassRange, PassAttk, PassPos}) :-
	ActRange >= abs(ActPos - PassPos),
	PassHpNew is PassHp - ActAttk.


% APPR
step(appr, {lefty, ActHp, ActRange, ActAttk, ActPos},
    {PassName, PassHp, PassRange, PassAttk, PassPos},
    {lefty, ActHp, ActRange, ActAttk, ActPosNew},
    {PassName, PassHp, PassRange, PassAttk, PassPos}) :-
	abs(PassPos - ActPos, Divider),
	Divider > 0,
	ActPosNew is ActPos + 1.

step(appr, {righty, ActHp, ActRange, ActAttk, ActPos},
    {PassName, PassHp, PassRange, PassAttk, PassPos},
    {righty, ActHp, ActRange, ActAttk, ActPosNew},
    {PassName, PassHp, PassRange, PassAttk, PassPos}) :-
	abs(PassPos - ActPos, Divider),
	Divider > 0,
	ActPosNew is ActPos - 1.


% WITHD
step(withd,  {lefty, ActHp, ActRange, ActAttk, ActPos},
    {PassName, PassHp, PassRange, PassAttk, PassPos},
    {lefty, ActHp, ActRange, ActAttk, ActPosNew},
    {PassName, PassHp, PassRange, PassAttk, PassPos}) :-
	ActPos > 0,
	ActPosNew is ActPos - 1.

step(withd,  {righty, ActHp, ActRange, ActAttk, ActPos},
    {PassName, PassHp, PassRange, PassAttk, PassPos},
    {righty, ActHp, ActRange, ActAttk, ActPosNew},
    {PassName, PassHp, PassRange, PassAttk, PassPos}) :-
	ActPos < 10,
	ActPosNew is ActPos + 1.


% max(ME, ENEMY, VALIDSTEPS, CURRSTEP OPTSTEPS, VALUE).
max(Depth, {MeName, MeHp, MeRange, MeAttk, MePos},
    {EnemyName, EnemyHp, EnemyRange, EnemyAttk, EnemyPos},
    CurrStep, [CurrStep|OptStepsCh], OptVal) :-
	Depth > 0,
	MeHp > 0, EnemyHp > 0,
	writef("\n_____ MAX _____\n", "MAX"),
	writeln(MeName), writeln(CurrStep),
	step(CurrStep, {MeName, MeHp, MeRange, MeAttk, MePos},
	    {EnemyName, EnemyHp, EnemyRange, EnemyAttk, EnemyPos},
	    MeNewState, EnemyNewState),
	steps_allowed(StepList),
	step_children(Depth - 1, min, MeNewState, EnemyNewState,
		      StepList, OptStepsCh, OptVal).

max(_, {_, MeHp, _, _, _}, _, _, [], -1) :-
	MeHp =< 0,
	writef("! ME DIED !\n").
max(_, _, {_, EnemyHp, _, _, _}, _, [], 1) :-
	EnemyHp =< 0,
	writef("! ENEMY DIED !\n").


% min(ME, ENEMY, VALIDSTEPS, CURRSTEP OPTSTEPS).
min(Depth, {MeName, MeHp, MeRange, MeAttk, MePos},
    {EnemyName, EnemyHp, EnemyRange, EnemyAttk, EnemyPos},
    CurrStep, [CurrStep|OptStepsCh], OptVal) :-
	Depth > 0,
	MeHp > 0, EnemyHp > 0,
	writef("\n_____ MIN _____\n", "MIN"),
	writeln(EnemyName), writeln(CurrStep),
	step(CurrStep, {EnemyName, EnemyHp, EnemyRange, EnemyAttk, EnemyPos},
	    {MeName, MeHp, MeRange, MeAttk, MePos}, EnemyNewState, MeNewState),
	steps_allowed(StepList),
	step_children(Depth - 1, max, MeNewState, EnemyNewState,
		      StepList, OptStepsCh, OptVal).

min(_, _, {_, EnemyHp, _, _, _}, _, [], -1) :-
	EnemyHp =< 0,
	writef("! ENEMY DIED !\n").
min(_, {_, MeHp, _, _, _}, _, _, [], 1) :-
	MeHp =< 0,
	writef("! ME DIED !\n").


% step_children(MaxOrMin, ACTOR, PASSIVE, VALIDSTEPS, OPTSTEPS, OPTVAL).
step_children(Depth, MaxOrMin, Act, Pass, [start], OptSteps, OptVal) :-
	steps_allowed(StepList),
	step_children(Depth, MaxOrMin, Act, Pass, StepList, OptSteps, OptVal).

step_children(Depth, max, Act, Pass, [VH|_], OptSteps, OptVal) :-
	Depth > 0,
	writef("\n__step_children_max1__\n"),
	Dtmp is Depth, writeln(Dtmp),
	writef("{name, hp, range, attk, pos}\n"),
	writeln(Act), writeln(Pass),
	max(Depth, Act, Pass, VH, OptStepsThis, OptValThis),
	append([], OptStepsThis, OptSteps), OptVal is OptValThis.
	%( OptValThis > OptValOther -> append([], OptStepsThis, OptSteps), OptVal is OptValThis ;
	%append([], OptStepsOther, OptSteps), OptVal is OptValOther ).

step_children(Depth, max, Act, Pass, [_|VT], OptSteps, OptVal) :-
	Depth > 0,
	writef("\n__step_children_max2__\n"),
	Dtmp is Depth, writeln(Dtmp),
	writef("{name, hp, range, attk, pos}\n"),
	writeln(Act), writeln(Pass),
	step_children(Depth, max, Act, Pass, VT, OptStepsOther, OptValOther),
	append([], OptStepsOther, OptSteps), OptVal is OptValOther.
	%( OptValThis > OptValOther -> append([], OptStepsThis, OptSteps), OptVal is OptValThis ;
	%append([], OptStepsOther, OptSteps), OptVal is OptValOther ).


step_children(Depth, min, Act, Pass, [VH|_], OptSteps, OptVal) :-
	Depth > 0,
	writef("\n__step_children_min1__\n"),
	Dtmp is Depth, writeln(Dtmp),
	writef("{name, hp, range, attk, pos}\n"),
        writeln(Act), writeln(Pass),
	min(Depth, Act, Pass, VH, OptStepsThis, OptValThis),
	append([], OptStepsThis, OptSteps), OptVal is OptValThis.
	%( OptValThis < OptValOther -> append([], OptStepsThis, OptSteps), OptVal is OptValThis ;
	%append([], OptStepsOther, OptSteps), OptVal is OptValOther ).

step_children(Depth, min, Act, Pass, [_|VT], OptSteps, OptVal) :-
	Depth > 0,
	writef("\n__step_children_min2__\n"),
	Dtmp is Depth, writeln(Dtmp),
	writef("{name, hp, range, attk, pos}\n"),
        writeln(Act), writeln(Pass),
	step_children(Depth, min, Act, Pass, VT, OptStepsOther, OptValOther),
	append([], OptStepsOther, OptSteps), OptVal is OptValOther.
	%( OptValThis < OptValOther -> append([], OptStepsThis, OptSteps), OptVal is OptValThis ;
	%append([], OptStepsOther, OptSteps), OptVal is OptValOther ).


step_children(_, _, _, _, [], [], 0).







